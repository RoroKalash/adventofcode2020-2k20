package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class day14 {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		Scanner sc = new Scanner(new File("src/sources/day14.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			lines.add(insert);
		}
		
		System.out.println(partOne(lines));
		System.out.println(partTwo(lines));

	}
	
	static long partOne(ArrayList<String> lines){
		
		String[] mask = new String[36];
		Map<String,String[]> memory = new HashMap<String,String[]>();
		long result = 0;
		
		for(String line : lines){
			String[] mask_mem = line.split(" = ");
			if(mask_mem[0].equals("mask")){
				for (int n = 0; n < mask_mem[1].length(); n++) {
				    mask[n] = String.valueOf(mask_mem[1].charAt(n));
				}
			}else{
				
				String[] addMem = new String[36];
				String binary = Integer.toBinaryString((Integer.valueOf(mask_mem[1])));
				binary = "000000000000000000000000000000000000".substring(binary.length()) + binary;
				
				for (int n = 0; n < binary.length(); n++) {
					if(mask[n].equals("X")){
					    addMem[n] = String.valueOf(binary.charAt(n));
					}else{
					    addMem[n] = mask[n];
					}
				}				
				
				if(!memory.containsKey(mask_mem[0])){
					memory.put(mask_mem[0], addMem);
				}else{
					memory.remove(mask_mem[0]);
					memory.put(mask_mem[0], addMem);
				}		
			}
		}
		
		
		for(Entry entrySet : memory.entrySet()){
			String temp = "";
			String [] val = (String[]) entrySet.getValue();
			for (int n = 0; n < val.length; n++) {
				temp+= val[n];
			}
			result += Long.parseLong(temp, 2);
		}

		return result;
		
	}

	static long partTwo(ArrayList<String> lines){
		
		String[] mask = new String[36];
		Map<Long,Long> memory = new HashMap<Long,Long>();
		long result = 0;
		
		for(String line : lines){
			String[] mask_mem = line.split(" = ");
			if(mask_mem[0].equals("mask")){
				for (int n = 0; n < mask_mem[1].length(); n++) {
				    mask[n] = String.valueOf(mask_mem[1].charAt(n));
				}
			}else{
				
				String[] addMem = new String[36];
				String adressMem = mask_mem[0].replace("mem[", "").replace("]", "");
				String binary = Integer.toBinaryString((Integer.valueOf(adressMem)));
				binary = "000000000000000000000000000000000000".substring(binary.length()) + binary;
				
				for (int n = 0; n < binary.length(); n++) {
					if(mask[n].equals("0")){
					    addMem[n] = String.valueOf(binary.charAt(n));
					}else{
					    addMem[n] = mask[n];
					}
				}
				
				ArrayList<String[]> putMem = new ArrayList<String[]>();
				putMem.add(addMem);
				
				for (int n = 0; n < addMem.length; n++) {
					if(addMem[n].equals("X")){
						ArrayList<String[]> nouvMem = new ArrayList<String[]>();
						for(String[] change : putMem){
							String[] nouv = Arrays.copyOf(change,change.length);
							change[n]="0";
							nouv[n]="1";
							nouvMem.add(nouv);
							}
						putMem.addAll(nouvMem);
					}
				}
				
				for(String[] ajouter : putMem){
					long adresse = 0;
					String temp = "";
					for (int n = 0; n < ajouter.length; n++) {
						temp+= ajouter[n];
					}
					adresse += Long.parseLong(temp, 2);
					if(!memory.containsKey(adresse)){
						memory.put(adresse, Long.valueOf(mask_mem[1]));
					}else{
						memory.remove(mask_mem[0]);
						memory.put(adresse, Long.valueOf(mask_mem[1]));
					}	
				}
				

				
			}
		}
		
		
		for(Entry entrySet : memory.entrySet()){
			long val = (Long) entrySet.getValue();
			result += val;
		}

		return result;
		
	}
	
	
	
}