package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class day15 {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		Scanner sc = new Scanner(new File("src/sources/day15.txt"));
		ArrayList<Integer> lines = new ArrayList<Integer>();
		Map<Integer,ArrayList<Integer>> chiffres = new HashMap<Integer,ArrayList<Integer>>();
		Map<Integer,ArrayList<Integer>> chiffres2 = new HashMap<Integer,ArrayList<Integer>>();

		int turn = 1;
		int enCours =0;
		
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			enCours = Integer.valueOf(insert);
			chiffres.put(enCours, new ArrayList<Integer>(Arrays.asList(turn)));
			chiffres2.put(enCours, new ArrayList<Integer>(Arrays.asList(turn++)));

		}
		
		int turn2 = turn;
		int enCours2 = enCours;
		
		System.out.println(partOne_Two(turn,chiffres,enCours,2021));
		System.out.println(partOne_Two(turn2,chiffres2,enCours2,30000001));

	}
	
	static long partOne_Two(int turn,Map<Integer,ArrayList<Integer>> chiffres,int enCours,int depart){
		
		while(turn != depart){
		if(chiffres.containsKey(enCours)){
			if(chiffres.get(enCours).size()==1){
				enCours=0;
				chiffres.get(enCours).add(turn++);
			}else{
				int diff = chiffres.get(enCours).get(chiffres.get(enCours).size()-1)-
						chiffres.get(enCours).get(chiffres.get(enCours).size()-2);
				enCours = diff;
				if(chiffres.containsKey(enCours)){
					chiffres.get(enCours).add(turn++);
				}else{
					chiffres.put(enCours,new ArrayList<Integer>(Arrays.asList(turn++)));
				}
			}
		}else{
			chiffres.put(enCours,new ArrayList<Integer>(Arrays.asList(turn++)));
		}
		}
		
		return enCours;
		
	}
}