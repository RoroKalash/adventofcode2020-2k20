package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class day19 {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		Scanner sc = new Scanner(new File("src/sources/day19.txt"));
		Map<String,String> rules = new HashMap<String,String>();
		String ticket = new String();
		ArrayList<String> exemples = new ArrayList<String>();

		int section =0;
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			if(insert.contains(":")) {
				String[] rule = insert.split(":");
				rules.put(rule[0].replace(" ", ""),rule[1]);
			}else {
				exemples.add(insert);
			}
		}
		
		System.out.println(partOne1(rules,exemples,"0"));

	}
	
	static long partOne1(Map<String,String> rules,ArrayList<String> exemples,String get){
		String replace = partOne(rules,exemples,get);
		int retour = 0;
		
		System.out.println(replace);
		
		for(String test : exemples) {
			if(test.matches(replace)) {
				System.out.println(test);
				retour++;
			}
		}
		
		
		return retour;
	}
	
	static String partOne(Map<String,String> rules,ArrayList<String> exemples,String get){
		String regex = rules.get(get);
		String replace= "";
		for(String rule : regex.split(" ")) {
			if(!rule.isBlank() && rule.contains("+")){
				replace += rule;
			}
			else if(!rule.isBlank() && !rule.equals("|") && rules.get(rule).replace("\"", "").replace(" ", "").matches("^[a-zA-Z\\|]$")){
				replace += rules.get(rule).replace("\"", "").replace(" ", "");
			}else if (!rule.isBlank() && rule.equals("|")){
				replace += "|";
			}else if (!rule.isBlank()) {
				replace += "("+partOne(rules,exemples,rule)+")";
			}
			
		}	
		return replace;
		
		 
	}
}