package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day13 {
	
public static void main(String[] args) throws FileNotFoundException {
			
	Scanner sc = new Scanner(new File("src/sources/day13.txt"));
	
		long depart =Long.valueOf(sc.nextLine());
		String ligne = sc.nextLine();
	
	System.out.println(partOne(depart,ligne));
	System.out.println(partTwo(ligne));
	
}

private static long partOne(long depart,String ligne) {
	
	String[] lignes = ligne.replace(",x", "").split(",");
	
	boolean nonTrouve=true;
	long ancien = depart;
	while(nonTrouve) {
		for(String busS : lignes) {
			long bus = Long.valueOf(busS);
			if(depart%bus == 0 ) {
				nonTrouve = false;
				return (depart-ancien)*bus;
			}
		}
	depart++;
	}
	return 0;
	
}

private static long partTwo(String ligne) {
	
	String[] buss = ligne.split(",");	
	ArrayList<Bus> lignes = new ArrayList<Bus>();
	
	for(int i=0;i<buss.length;i++) {
		Bus insert = new Bus();
		if(!buss[i].equals("x")) {	
			insert.index = Integer.valueOf(buss[i])-i;
			insert.nbLig = buss[i];
			lignes.add(insert);
		}
	}
	
	
    long produit = lignes.stream()
            .mapToLong(i -> Long.valueOf(i.nbLig))
            .reduce(1, (a, b) -> a * b);
    long somme = 0;	
	
    for (int i = 0; i < lignes.size(); i++) {
        long result = produit / Long.valueOf(lignes.get(i).nbLig);
        long inverse = BigInteger.valueOf(result)
                .modInverse(BigInteger.valueOf(Long.valueOf(lignes.get(i).nbLig)))
                .longValue();
        somme += result * inverse * lignes.get(i).index;

    }
    
    return somme%produit;
}

}

class Bus{
	int index;
	String nbLig;
	
	Bus(){
	}
}

