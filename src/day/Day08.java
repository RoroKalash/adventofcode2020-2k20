package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day08 {
	
	public static void main(String[] args) throws FileNotFoundException {
				
		Scanner sc = new Scanner(new File("src/sources/day08.txt"));
		ArrayList<Ins> def = new ArrayList<Ins>();
	
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			Ins ins = new Ins();
			ins.instructions = insert;
			def.add(ins);
		}
		
		
		System.out.println(partOne(def));
		System.out.println(partTwo(def));
		
	}
	
	public static int partOne(ArrayList<Ins> def) {
							
			ArrayList<Ins> lines = new ArrayList<Ins>();
			
		    for (Ins ins : def) {
		    	Ins ins2 = new Ins();
		    	ins2.instructions=ins.instructions;
		    	ins2.dejaVue=ins.dejaVue;
		    	lines.add(ins2);
		    }	
		    
			return partieCommune(lines,false);
	}
	
	public static int partTwo(ArrayList<Ins> def) {
		
		for(int j=0;j<def.size();j++) {
			
			ArrayList<Ins> lines = new ArrayList<Ins>();
			
		    for (Ins ins : def) {
		    	Ins ins2 = new Ins();
		    	ins2.instructions=ins.instructions;
		    	ins2.dejaVue=ins.dejaVue;
		    	lines.add(ins2);
		    }	
		    
			if(lines.get(j).instructions.substring(0, 3).equals("jmp")) {
				lines.get(j).instructions = lines.get(j).instructions.replace("jmp", "nop");
			}else if(lines.get(j).instructions.substring(0, 3).equals("nop")) {
				lines.get(j).instructions = lines.get(j).instructions.replace("nop", "jmp");
			}

			int somme=partieCommune(lines,true);
			if(somme != 0) {
				return somme;
			}
		}
		return 0;
	}
	
	
	public static int partieCommune(ArrayList<Ins> lines,boolean partTwo) {
		int i =0;
		boolean terminer = false;	
		int somme = 0;
		
		
		while( !terminer && i<lines.size()){
			
			if(lines.get(i).dejaVue) {
				terminer = true;
			}else {
				
				lines.get(i).dejaVue = true;
			
				if(lines.get(i).instructions.substring(0, 3).equals("acc")) {
					if(lines.get(i).instructions.charAt(4) == '+') {
						somme += Integer.valueOf(lines.get(i).instructions.substring(5));
					}else {
						somme -= Integer.valueOf(lines.get(i).instructions.substring(5));
					}
					i++;
				}else if(lines.get(i).instructions.substring(0, 3).equals("jmp")) {
					if(lines.get(i).instructions.charAt(4) == '+') {
						i += Integer.valueOf(lines.get(i).instructions.substring(5));
					}else {
						i -= Integer.valueOf(lines.get(i).instructions.substring(5));
					}			
				}else {
					i++;
				}
			}
		}
		
		if(partTwo) {
			if(i == lines.size()) {
				return somme;
			}else {
				return 0;

			}
		}else {
			return somme;
		}
	}
}

class Ins{
	String instructions = "";
	boolean dejaVue = false;
}
