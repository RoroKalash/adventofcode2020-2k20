package day;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day10 {
	
public static void main(String[] args) throws FileNotFoundException {
			
	Scanner sc = new Scanner(new File("src/sources/day10.txt"));
	ArrayList<Integer> def = new ArrayList<Integer>();
	
	while(sc.hasNextLine()){
		String insert =sc.nextLine();
		def.add(Integer.valueOf(insert));
	}
	
	Collections.sort(def);
	
	System.out.println(partOne(def));
	System.out.println(partTwo(def));
	
}

private static int partOne(ArrayList<Integer> def) {
    ArrayList<Integer> unEtTrois = UnEtTrois(def);
    int un = 0;
    int trois = 0;
    
    for(Integer result : unEtTrois) {
    	if(result.intValue() == 1) {
    		un++;
    	}else {
    		trois++;
    	}
    }
	return un*(trois);
}


public static long partTwo(ArrayList<Integer> def) {
	int count=0;
    ArrayList<Integer> retour = new ArrayList<>();
	
    ArrayList<Integer> unEtTrois = UnEtTrois(def);
    for(Integer result : unEtTrois) {
    	if(result.intValue() == 1) {
    		count++;
    	}else if(count !=0 ){
    		retour.add((((count-1)*count)/2)+1);
    		count=0;
    	}
    }
    
    long ret = 1;
    for(Integer it : retour) {
    	ret*=it;
    }
    return ret;

}

public static ArrayList<Integer> UnEtTrois(ArrayList<Integer> def) {
    int ref = 0;
    ArrayList<Integer> unEtTrois = new ArrayList<>();
    
    for (Integer i : def) {
        if ((i-ref)==1) {
            unEtTrois.add(1);
        } else if ((i-ref)==3) {
        	unEtTrois.add(3);
        }
        ref = i;
    }
    
    unEtTrois.add(3);
    
    return unEtTrois;	
}

}

