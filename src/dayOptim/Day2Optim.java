package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import Utils.Pair;

public class Day2Optim {
	
	static Pair total = new Pair();

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("src/sources/day2.txt"));
		
		while(sc.hasNextLine()){
			String ligne = sc.nextLine();
			String[] result = ligne.split(" ");
			String[] borne = result[0].split("-");
			int borne1 = Integer.valueOf(borne[0]);
			int borne2 = Integer.valueOf(borne[1]);

			partOneTwo(borne1,borne2,result[1].substring(0, 1),result[2]);
			
		}

        System.out.println(total.partOne);
        System.out.println(total.partTwo);

	}
	
	static void partOneTwo(int borne1,int borne2,String lettre,String mot){

			int count = mot.length() - 
					mot.replace(lettre, "").length();

	        if(borne1<=count  && count <=borne2){
	        	total.partOne++;
	        }
	        
	        if((mot.charAt(borne1-1) == lettre.charAt(0) && mot.charAt(borne2-1) != lettre.charAt(0)) ||
	        		(mot.charAt(borne1-1) != lettre.charAt(0) && mot.charAt(borne2-1) == lettre.charAt(0))){
	        	total.partTwo++;
	        }   
	      
	}

}
