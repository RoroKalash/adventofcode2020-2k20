package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import Utils.Pair;

public class Day3Optim {
	
	static Pair total = new Pair();


	public static void main(String[] args) throws FileNotFoundException {
		
		int sumArbre1 = 0;
		int sumArbre2 = 0;
		int sumArbre3 = 0;
		int sumArbre4 = 0;
		int sumArbre5 = 0;
		int curs = 0;
		int curs1 = 0;
		int curs2 = 0;
		int curs3 = 0;
		int curs4 = 0;
		int curs5 = 0;
		int i=0;
		
		Scanner sc = new Scanner(new File("src/sources/day3.txt"));
		ArrayList<String> lines = new ArrayList<String>();
		while(sc.hasNextLine()){
			String ligne = sc.nextLine();
			
			if(ligne.charAt(curs1%ligne.length())=='#'){
				sumArbre1++;
			}
			curs1+=1;
			if(ligne.charAt(curs2%ligne.length())=='#'){
				sumArbre2++;
				total.partOne++;
			}
			curs2+=3;
			if(ligne.charAt(curs3%ligne.length())=='#'){
				sumArbre3++;
			}
			curs3+=5;
			if(ligne.charAt(curs4%ligne.length())=='#'){
				sumArbre4++;
			}
			curs4+=7;
			
			if(i%2==0) {
				if(ligne.charAt(curs5%ligne.length())=='#'){
					sumArbre5++;
				}
				curs5+=1;	
			}
		
			i++;
		}
		
		System.out.println(total.partOne);
		System.out.println(BigDecimal.valueOf(sumArbre1*sumArbre2*sumArbre3*sumArbre4).multiply(BigDecimal.valueOf(sumArbre5)));

	}
	
	static void partOneTwo(String ligne){}
}
