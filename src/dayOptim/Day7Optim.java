package dayOptim;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day7Optim {
	
	private static Map<String, ArrayList<String>> tapis = new HashMap<String,ArrayList<String>>();
	
	public static void main(String[] args) throws FileNotFoundException {
		
		
		Scanner sc = new Scanner(new File("src/sources/day7.txt"));
		ArrayList<String> lines = new ArrayList<String>();
	
		while(sc.hasNextLine()){
			String insert =sc.nextLine();
			lines.add(insert);
		}
				
		for(String contraintes : lines){
			
			String[] bagages = contraintes.split(" bags contain ");
			String contenant = bagages[0];
			String[] contenu = bagages[1].split("\\s*( bag, )|( bags, )|( bags.)|( bag.)\\s*");
						
	        if(tapis.containsKey(contenant)){
	        	for(String cont :contenu) {
	                tapis.get(contenant).add(cont);
	        	}
	        }else {
	        	ArrayList<String> contS = new ArrayList<String>();
	        	for(String cont :contenu) {
	        		cont = cont.replace("no other", "0 other");
	        		contS.add(cont);
	        	}
		        tapis.put(contenant, contS);
	        }
		}
		
		System.out.println(partOne());
		System.out.println(partTwo());
			
	}
	
	static int partOne(){
		 
        int numberInside = 0;
    	
        for (Map.Entry<String, ArrayList<String>> entry : tapis.entrySet()) {
            if(calculateBags(entry.getKey())) {
            	numberInside++;
            }
    	} 
    	
		return numberInside;
		
		
	}
	
    public static boolean calculateBags(String center) {

        if (tapis.containsKey(center)) {
        	ArrayList<String> bags = tapis.get(center);
            for (String bag : bags) {
            	if(bag.contains("shiny gold")) {
            		return true;
            	}else {
                	if(calculateBags(bag.substring(2))){
                		return true;
                	}
            	}
            }
        }
		return false;
    }
    
	static int partTwo(){
		 
	    int numberInside = 0;
	    numberInside = calculateBags2("shiny gold");
		
		return numberInside;
	
	}
	
    public static int calculateBags2(String center) {
    	int numberBag = 0;

        if (tapis.containsKey(center)) {
        	ArrayList<String> bags = tapis.get(center);
            for (String bag : bags) {
            	if(bag.charAt(0) != 'n') {
                	numberBag += Integer.valueOf(bag.substring(0,1)) + Integer.valueOf(bag.substring(0,1))*calculateBags2(bag.substring(2));
            	}
            }
        }
		return numberBag;
    }
}
